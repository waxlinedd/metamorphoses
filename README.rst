..
    Convert this document to html with "rst2html README.rst > README.html".
    Extract examples to examples.py with "python make_examples.py".

=============
Metamorphoses
=============

.. contents:: Table of Contents

Overview
========

Metamorphoses are changes of shape.

As a class matures, its shape changes: fields may be added or removed,
or a field's semantics may change.  If objects of the class are
serialized and perhaps saved to a file, these objects will have to be
converted when they are deserialized if the shape of the class has
been changed since serialization.

Metamorphoses helps you manage the changing shape of your class by
allowing you to specify all the fields for each version of the class
and how to convert between the versions.  This explicit specification
of all fields also allows other features to be automatically provided.

Metamorphoses supports:

* Explicit class versions
* Separation of versioning concerns from the class definition
* Inheritance
* Conversion between versions (forwards and backwards)
* Conversion from other serialization/versioning schemes
* Automatically generated __getstate__() and __setstate__() for pickle
* Automatically generated but customizable __init__()
* Automatically generated __repr__()
* Automatically generated comparison dunder methods
* Automatically generated __slots__



Basic Usage
===========

To define the versions for your class, subclass Metamorphoses and fill
out the versions dict.  The versions dict keys are version ids, and
values are the versions.  A Version may consist of any number of Attrs
which are attribute names and default values:

.. code:: python

  from __future__ import print_function
  from metamorphoses import Attr, Base, FutureAttr, Metamorphoses, Version

  class Example1Versions(Metamorphoses):
      versions = {}
      versions[0] = Version(Attr(name='foo', default_value=1),
			    Attr(name='bar', default_value=42))

Now that a version is defined for your class, apply the Metamorphoses
(just *one* shape at this point) to your class via decoration:

.. code:: python

  @Example1Versions
  class Example1(object):
      pass

  print(Example1(bar=3))

The above will print::

  Example1(foo=1, bar=3)


Conversion
==========

Suppose you misspell an attribute name or wish to change the semantics
of an attribute.  Create a new version with the proper name and
semantics, and provide a convert method that converts between the
versions:


.. code:: python

  import math

  class Example2Versions(Metamorphoses):
      versions={}
      versions[0] = Version(Attr('angel', 180.))
      versions[1] = Version(Attr('angle', math.pi))

      @classmethod
      def convert(cls, obj, old_version_id, new_version_id):
	  if old_version_id < 1 and new_version_id >= 1:
	      # We changed the name of a variable and its semantics.
	      obj.angle = math.radians(obj.angel)
	      del obj.angel

  @Example2Versions
  class Example2(object):
      pass

We can test that this works by creating an object of the old version,
serializing and deserializing it, and printing the deserialized object
to see if it has been converted up during deserialization:

.. code:: python

  e2 = Example2Versions.factory(0, Example2, angel=360.)
  old_e2_state = Example2Versions.getstate(0, e2)
  e2.__setstate__(old_e2_state)

  print(e2)



Inheritance
===========

There two ways of handling inheritance.  You may specify the base class,
or specify a version of a base class.

If you specify the base class:
 * Converts of the base class will be handled by the base class convert.
 * Base class attributes will not be included in the generated subclass __init__() arguments.
 * Base class attributes will not be included in generated subclass __repr__() output.
 * Generated comparison functions do not yet include base class attributes.

(The subclass __init__() and __repr__() may be customized to include
base class attributes as needed.)

If you specify a base class version:
 * Converts of base classes must be handled by the subclass.
 * Base class attributes will be included in the generated subclass __init__() arguments.
 * Base class attributes will be included in generated subclass __repr__() output.

You should usually specify a base class.  You should only specify a base
class version if you are okay with bumping your subclass version and
updating its convert() every time the base class changes.

An example of specifying a base class:

.. code:: python

  class Example3Versions(Metamorphoses):
      versions = {0:Version(Base(Example1),
                            Base(Example2))}

  @Example3Versions
  class Example3(Example1, Example2):
      pass

  print(Example3())



An example of specifying a base class version:

.. code:: python

  class Example4Versions(Metamorphoses):
      versions = {0:Version(Base(Example1Versions, 0),
                            Base(Example2Versions, 1))}

  @Example4Versions
  class Example4(Example1, Example2):
      pass

  print(Example4())



Custom __init__()
=================

The automatically generated __init__() method gives the calling object
all the specified attributes assigned with the default values.  Suppose
you need to initialize a non-serialized attribute in __init__().  You
will have to define your own __init__(), but you can still use a
generated function to initialize all attributes specified in the
current Version:

.. code:: python

  class Example5Versions(Metamorphoses):
      versions = {0:Version(Attr('feet', 5280.))}

  @Example5Versions
  class Example5(object):
      _init = Example5Versions.init_factory()
      def __init__(self, *args, **kwargs):
	  self._init(*args, **kwargs)
	  self.miles = self.feet/5280.

  print(Example5())


Adding Backwards Compatible Fields
==================================

Suppose you are in the middle of a release cycle.  You need to add a
field to your class, but you can't make serialization changes.  No
problem!  Add your new attribute to the current Version with
FutureAttr.  If you include the current Version in the next Version,
the FutureAttr will be automatically promoted to a regular Attr in
that Version.

Here we started out with the single attribute 'foo', added 'bar' in
the middle of a release cycle, and then in the next version kept all
the attributes of Version 0 and added attribute 'baz':

.. code:: python

  class Example6Versions(Metamorphoses):
      versions = {}
      versions[0] = Version(Attr('foo', 0), FutureAttr('bar', 1))
      versions[1] = Version(versions[0], Attr('baz', 2))

  @Example6Versions
  class Example6(object):
      pass

  print(Example6())

Automatic __slots__
===================

.. code:: python

   class Example7Versions(Metamorphoses):
       make_slots = True
       versions={}
       versions[1] = Version(Attr('foo', 0))

   @Example7Versions
   class Example7(object):
       pass

   try:
       bar = Example7()
       bar.bar = 'nope'
       print(bar)
   except Exception, ex:
       print(ex.message)

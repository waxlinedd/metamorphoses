from metamorphoses import Attr, Base, FutureAttr, Metamorphoses
from metamorphoses import MetamorphosesException, Version
import unittest

class TestBasics(unittest.TestCase):
    ''' If lots of tests fail, check these first.'''
    def setUp(self):
        class Versions(Metamorphoses):
            versions={1:Version(Attr('foo', 10)),
                      2:Version(Attr('bar', 20))}
            @classmethod
            def convert(cls, obj, old_version_id, new_version_id):
                if old_version_id < 2:
                    obj.bar = obj.foo
                    del obj.foo

            def implicit_classmethod(cls):
                pass

        @Versions
        class Example(object):
            pass

        self.Versions = Versions
        self.Example = Example

        self.one = Example(1)
        self.two = Example(2)
        self.too = Example(2)


    def test_current_version(self):
        self.assertEqual(self.Versions.current_version(),
                         self.Versions.versions[2])

    def test_default_version(self):
        self.assertEqual(self.Versions.current_version_id, 2)

    def test_default_init(self):
        ex = self.Example()
        self.assertEqual(ex.bar, 20)

    def test_default_init_with_arg(self):
        ex = self.Example(42)
        self.assertEqual(ex.bar, 42)

    def test_default_init_with_kwarg(self):
        ex = self.Example(bar=42)
        self.assertEqual(ex.bar, 42)

    def test_default_init_invalid_kwarg(self):
        try:
            ex = self.Example(foo=42)
            ok = False
        except:
            ok = True
        self.assertEqual(ok, True)

    def test_convert(self):
        ex = self.Versions.factory(1, self.Example, foo=42)
        state = self.Versions.getstate(1, ex)
        self.Versions.setstate(2, ex, state)
        self.assertEqual(ex.bar, 42)

    def test_implicit_classmethod(self):
        try:
            self.Versions.implicit_classmethod()
            ok = True
        except:
            ok = False
        self.assertEqual(ok, True)

    def test_repr(self):
        ex = self.Example(bar=42)
        self.assertEqual(repr(ex), 'Example(bar=42)')

    def test_lt(self):
        self.assertTrue(self.one < self.two)
        self.assertFalse(self.two < self.one)

    def test_lel(self):
        self.assertTrue(self.one <= self.two)
        self.assertFalse(self.two <= self.one)

    def test_lee(self):
        self.assertTrue(self.two <= self.too)

    def test_gt(self):
        self.assertTrue(self.two > self.one)
        self.assertFalse(self.one > self.two)

    def test_geg(self):
        self.assertTrue(self.two >= self.one)

    def test_gee(self):
        self.assertTrue(self.two >= self.too)
        self.assertFalse(self.one >= self.two)

    def test_eq(self):
        self.assertTrue(self.two == self.too)
        self.assertFalse(self.one == self.two)

    def test_ne(self):
        self.assertTrue(self.one != self.two)
        self.assertFalse(self.two != self.too)

class TestSubclassWithBaseVersions(unittest.TestCase):
    def setUp(self):
        class Versions1(Metamorphoses):
            versions={1:Version(Attr('foo1', 10)),
                      2:Version(Attr('bar1', 20))}
            @classmethod
            def convert(cls, obj, old_version_id, new_version_id):
                if old_version_id < 2:
                    obj.bar1 = obj.foo1
                    del obj.foo1

            def implicit_classmethod(cls):
                pass

        @Versions1
        class Example1(object):
            pass

        class Versions2(Metamorphoses):
            versions={1:Version(Attr('foo2', 10)),
                      2:Version(Attr('bar2', 20))}
            @classmethod
            def convert(cls, obj, old_version_id, new_version_id):
                if old_version_id < 2:
                    obj.bar2 = obj.foo2
                    del obj.foo2

            def implicit_classmethod(cls):
                pass

        @Versions2
        class Example2(object):
            pass

        class Versions(Metamorphoses):
            versions={1:Version(Base(Versions1, 1),
                                Base(Versions2, 1),
                                Attr('foo', 10)),
                      2:Version(Base(Versions1, 2),
                                Base(Versions2, 2),
                                Attr('bar', 20))}
            @classmethod
            def convert(cls, obj, old_version_id, new_version_id):
                if old_version_id < 2:
                    obj.bar = obj.foo
                    del obj.foo

            def implicit_classmethod(cls):
                pass

        @Versions
        class Example(Example2, Example1):
            pass

        self.Versions = Versions
        self.Example = Example

        self.one = Example(bar2=1)
        self.two = Example(bar2=2)
        self.too = Example(bar2=2)

    def test_default_init_with_arg(self):
        ex = self.Example(42)
        self.assertEqual(ex.bar1, 42)

    def test_default_init_with_kwarg(self):
        ex = self.Example(bar=42)
        self.assertEqual(ex.bar, 42)

    def test_lt(self):
        self.assertTrue(self.one < self.two)

    def test_lel(self):
        self.assertTrue(self.one <= self.two)

    def test_lee(self):
        self.assertTrue(self.two <= self.too)

    def test_gt(self):
        self.assertTrue(self.two > self.one)

    def test_geg(self):
        self.assertTrue(self.two >= self.one)

    def test_gee(self):
        self.assertTrue(self.two >= self.too)

    def test_eq(self):
        self.assertTrue(self.two == self.too)

    def test_ne(self):
        self.assertTrue(self.one != self.two)

class TestSubclassWithBases(unittest.TestCase):
    def setUp(self):
        class Versions1(Metamorphoses):
            versions={1:Version(Attr('foo1', 10))}

            def implicit_classmethod(cls):
                pass

        @Versions1
        class Example1(object):
            pass

        class Versions2(Metamorphoses):
            versions={1:Version(Attr('foo1', 10)),
                      2:Version(Attr('foo2', 20))}
            @classmethod
            def convert(cls, obj, old_version_id, new_version_id):
                if old_version_id < 2:
                    obj.foo2 = obj.foo1
                    del obj.foo1

            def implicit_classmethod(cls):
                pass

        @Versions2
        class Example2(object):
            pass

        # Derived1 and Derived 2 are the same but for Base classes where
        # Example2 can convert Example1.
        class Derived1Versions(Metamorphoses):
            versions={1:Version(Base(Example1),
                                Attr('foo', 10)),}

        class Derived2Versions(Metamorphoses):
            versions={1:Version(Base(Example2),
                                Attr('foo', 20)),}


        @Derived1Versions
        class Derived1(Example1):
            pass

        @Derived2Versions
        class Derived2(Example2):
            pass

        self.Derived1 = Derived1
        self.Derived2 = Derived2

    def test_init_base_class(self):
        ex = self.Derived1()
        self.assertEqual(ex.foo1, 10)

    def test_default_init_with_arg(self):
        ex = self.Derived1(42)
        self.assertEqual(ex.foo, 42)

    def test_default_init_with_kwarg(self):
        ex = self.Derived1(foo=42)
        self.assertEqual(ex.foo, 42)

    def test_convert(self):
        d1 = self.Derived1(42)
        d1.foo1 = 10
        d2 = self.Derived2(42)
        d2.foo2 = 20
        d1_state = d1.__getstate__()
        d2.__setstate__(d1_state)
        self.assertEqual(d2.foo2, 10)

class TestFutureAttrs(unittest.TestCase):
    def setUp(self):
        class Versions0(Metamorphoses):
            versions={}
            versions[0] = Version(Attr('foo', 10))

        class Versions1(Metamorphoses):
            versions={}
            versions[0] = Version(Attr('foo', 20), FutureAttr('bar', 30))

        @Versions0
        class Example0(object):
            pass

        @Versions1
        class Example1(object):
            pass

        self.Versions0 = Versions0
        self.Versions1 = Versions1
        self.Example0 = Example0
        self.Example1 = Example1
        self.e0 = Example0()
        self.e1 = Example1()

    def test_setstate(self):
        e1_state = self.e1.__getstate__()
        self.e0.__setstate__(e1_state)
        self.assertEqual(self.e0.foo, 20)
        self.assertEqual(self.e0.bar, 30)

    def test_getstate(self):
        e1_state = self.e1.__getstate__()
        self.e0.__setstate__(e1_state)
        e0_state = self.e0.__getstate__()
        self.assertEqual(e0_state, e1_state)


class TestNoReserializeFutureAttrs(unittest.TestCase):
    def setUp(self):
        class Versions0(Metamorphoses):
            reserialize_future_attrs = False
            versions={}
            versions[0] = Version(Attr('foo', 10))

        class Versions1(Metamorphoses):
            versions={}
            versions[0] = Version(Attr('foo', 20), FutureAttr('bar', 30))

        @Versions0
        class Example0(object):
            pass

        @Versions1
        class Example1(object):
            pass

        self.Versions0 = Versions0
        self.Versions1 = Versions1
        self.Example0 = Example0
        self.Example1 = Example1
        self.e0 = Example0()
        self.e1 = Example1()

    def test_setstate(self):
        e1_state = self.e1.__getstate__()
        self.e0.__setstate__(e1_state)
        self.assertEqual(self.e0.foo, 20)
        self.assertEqual(self.e0.bar, 30)

    def test_getstate(self):
        e1_state = self.e1.__getstate__()
        self.e0.__setstate__(e1_state)
        e0_state = self.e0.__getstate__()
        self.assertEqual(e0_state, (0,(20,),{}))


if __name__ == '__main__':
    unittest.main()

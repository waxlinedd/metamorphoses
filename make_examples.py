if __name__ == '__main__':
    from docutils.core import publish_doctree
    import os

    def is_code_block(node):
        return (node.tagname == 'literal_block'
                and 'code' in node.attributes['classes'])

    readme = open(os.path.dirname(__file__) + 'README.rst').read()
    doctree = publish_doctree(readme)
    code_blocks = doctree.traverse(condition=is_code_block)

    examples = open(os.path.dirname(__file__) + 'examples.py', 'w')
    for block in code_blocks:
        for line in block.astext().splitlines():
            examples.write(line + '\n')
        examples.write('\n\n')

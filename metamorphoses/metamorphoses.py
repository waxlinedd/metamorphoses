'''
Todo:

    * Put allow_future_attributes in Version, defaulting to
      Metamorphoses allow_future_attributes value.
    * Review lt(), eq()
    * Review conversion to earlier version, write tests
    * Write tests for future_attrs.
    * Review for other untested features.
    * Cover all features in README.
    * Support explicit typing?
    * Allow removal of attributes in a previous Version?
    * Allow overriding attributes in a previous Version?
    * Allow updating version of Base introduced in a previous Version?

'''
from collections import namedtuple
from functools import total_ordering
import copy
import types

class MetaMetamorphoses(type):
    '''Metaclass for Metamorphoses

    Set current_version_id to the max version_id if current_version_id is unset.

    Make the class a decorator and not instantiable.

    Make member functions into classmethods.

    '''
    def __new__(meta, cls_name, cls_bases, cls_dict):
        # Set current_version_id if there isn't one already.
        if 'current_version_id' not in cls_dict and \
           'versions' in cls_dict and \
           len(cls_dict['versions']):
            cls_dict['current_version_id'] = max(cls_dict['versions'])

        # Turn functions into classmethods.
        for key, value in cls_dict.items():
            if type(value) is types.FunctionType:
                cls_dict[key] = classmethod(value)

        cls = super(MetaMetamorphoses, meta).__new__(
            meta, cls_name, cls_bases, cls_dict)
        return cls

    # Metamorphoses is a decorator and not instantiable.
    def __call__(cls, decorated_cls):
        return cls.decorate(cls.current_version_id, decorated_cls)


class Metamorphoses(object):
    '''
    Metamorphoses is a base class for decorators which add version support
    for serialization and optionally generate dunder methods.

    Attributes:
        make_init (bool): Generate an __init__() for decorated classes.
        make_repr (bool): Generate a __repr__() for decorated classes.
        make_cmp (bool): Generate comparison dunder methods for decorated classes.
        make_slots (bool): Generate __slots__ for decorated classes.
        allow_future_attrs (bool): Allow FutureAttrs to be serialized/deserialized.
           Use this to prevent unnecessarily storing an empty FutureAttrs dict
           if it will never be needed.
        reserialize_future_attrs (bool):
        init_bases: Call base class __init__() for Bases with no version.
    '''
    __metaclass__ = MetaMetamorphoses

    make_getstate = True
    make_setstate = True
    make_boost_python_compatible = True
    make_init = True
    make_repr = True
    make_cmp = True
    make_slots = False
    allow_future_attrs = True
    reserialize_future_attrs = True
    init_bases = True

    @classmethod
    def current_version(cls):
        '''Get the current/default Version for this Metamorphoses.'''
        return cls.versions[cls.current_version_id]


    @staticmethod
    def get_attrs_state(obj, attrs):
        return tuple(getattr(obj, attr.name) for attr in attrs)

    @staticmethod
    def get_bases_state(obj, bases):
        return tuple(base.cls.__getstate__(obj)
                     for base in bases if not base.has_version)

    @classmethod
    def getstate(cls, version_id, obj):
        '''Get the serializable state of an object.

        Args:
            version_id: version_id of the Version of the obj.
            obj: object to get serializable state from.

        Returns:
            tuple: Serializable state of obj.

            The tuple has two to four values:

            1. version_id

            2. A tuple of values for each Attr in
               cls.versions[version_id].attrs in the given order. If
               Bases have explicitly specified versions, their attrs are
               included.

            3. A dict of attribute names/values for attributes specified
               in cls.versions[version_id].future_attrs.  This tuple
               value is absent if cls.allow_future_attrs == False.

            4. A tuple of Bases serializable state. This tuple value is
               absent if all Bases have specified a base class version
               rather than a base class.

        '''
        version = cls.versions[version_id]
        attrs_state = cls.get_attrs_state(obj, version.attrs)

        state = [version_id, attrs_state]

        future_state = None
        if cls.allow_future_attrs:
            future_state = {attr.name: getattr(obj, attr.name)
                            for attr in version.future_attrs}
            if cls.reserialize_future_attrs and \
               hasattr(obj, '__metamorphoses_future_attrs__'):
                for name in obj.__metamorphoses_future_attrs__:
                    future_state[name] = getattr(obj, name)

        elif len(version.future_attrs):
            raise MetamorphosesException(('%s does not allow FutureAttrs '+
                                          'but FutureAttrs are specified.')
                                         % cls.__name__)

        if future_state is not None:
            state.append(future_state)

        base_states = cls.get_bases_state(obj, version.bases)
        if len(base_states):
            state.append(base_states)

        return tuple(state)


    @classmethod
    def setstate(cls, version_id, obj, args):
        '''Set the attributes of an object from its serializable state.

        Args:
            version_id: version_id of the Version of the obj.
            obj: object on which to set attributes.
            args: serializable state from which to set obj attributes.
        '''
        raw_state = cls.convert_raw_state(args, version_id)

        serialized_version_id = raw_state[0]
        attrs_state = raw_state[1]
        future_state = {}
        bases_state = tuple()

        if len(raw_state) > 2:
            if type(raw_state[2]) == dict:
                future_state = raw_state[2]
            else:
                bases_state = raw_state[2]

        if len(raw_state) == 4:
            bases_state = raw_state[3]

        if len(future_state) and not cls.allow_future_attrs:
            raise MetamorphosesException(
                'FutureAttrs in serialization of %s, but none are expected.' %
                cls.__name__)

        try:
            serialized_version = cls.versions[ serialized_version_id ]
        except KeyError:
            raise MetamorphosesException(
                ('Unknown serialized version_id for %s: %s.\n' +
                 'Available versions: %s')
                % (cls.__name__, serialized_version_id,
                   cls.versions.keys()))

        if len(bases_state):
            bases = [base for base in serialized_version.bases
                     if not base.has_version]
            if len(bases) != len(bases_state):
                raise MetamorphosesException(
                    'Incorrect number of base states: %d/%d/%d' %
                    (len(bases_state), len(bases), len(serialized_version.bases)))
            for base, base_state in zip(bases, bases_state):
                base.cls.__setstate__(obj, base_state)

        try:
            version = cls.versions[ version_id ]
        except KeyError:
            raise MetamorphosesException(
                ('Unknown version_id for %s: %s.\n' +
                 'Available versions: %s')
                % (cls.__name__, version_id,
                   cls.versions.keys()))

        serialized_attrs = serialized_version.attrs

        # Initialize any new attributes.
        for attr in cls.versions[version_id].all_attrs:
            setattr(obj, attr.name, copy.deepcopy(attr.default_value))

        # Set serialized state attributes.
        if len(attrs_state) != len(serialized_attrs):
            raise MetamorphosesException(
                ('Wrong attrs_state size for %s version %s.\n' +
                 'Correct size is %d.\n' +
                 'Actual size is %d.')
                % (cls.__name__, serialized_version_id,
                   len(serialized_attrs), len(attrs_state)))
        for ii in range(len(serialized_attrs)):
            setattr(obj, serialized_attrs[ii].name, attrs_state[ii])

        if cls.allow_future_attrs:
            for key, value in future_state.items():
                setattr(obj, key, value)

            if cls.reserialize_future_attrs:
                future_attr_names = future_state.keys()
                future_attr_names.sort()
                if version.future_attr_names() != future_attr_names:
                    obj.__metamorphoses_future_attrs__ = [
                        name for name in future_attr_names
                        if name not in version.future_attr_names()]


        # Convert from serialized version to current version.
        if serialized_version_id != version_id:
            cls.convert(obj, serialized_version_id, version_id)

        # End setstate()

    @classmethod
    def getstate_factory(cls, version_id=None):
        if version_id is None:
            version_id = cls.current_version_id
        return lambda obj: cls.getstate(version_id, obj)

    @classmethod
    def setstate_factory(cls, version_id=None):
        if version_id is None:
            version_id = cls.current_version_id
        return lambda obj, state: cls.setstate(version_id, obj, state)

    @classmethod
    def repr_args(cls, version_id, obj):
        '''Create argument string for use by __repr__().

        Args:
            version_id: version id for the Version of obj.
            obj: object whose attributes are used to generate the argument string.

        Returns:
            str: A representation of the attributes of obj.
        '''
        args = [ attr.name + '=' + repr(getattr(obj, attr.name))
                 for attr in cls.versions[version_id].all_attrs]

        return ', '.join(args)

    @classmethod
    def repr(cls, version_id, obj):
        '''Create a string representation of an object.

        Args:
            version_id: version id for the Version of obj.
            obj: object for which to generate a string representation.

        Returns:
            str: A representation of obj.
        '''
        return obj.__class__.__name__ + \
            '(' + cls.repr_args(version_id, obj) + ')'

    # FIXME: Is the order of classes stable?
    @staticmethod
    def lt(lhs, rhs):
        return (lhs.__getstate__(), lhs.__class__) < \
               ((rhs.__getstate__(), rhs.__class__)
                if hasattr(rhs, '__getstate__')
                else rhs)

    @staticmethod
    def eq(lhs, rhs):
        return (lhs.__getstate__(), lhs.__class__) == \
               ((rhs.__getstate__(), rhs.__class__)
                if hasattr(rhs, '__getstate__')
                else rhs)

    @classmethod
    def convert_raw_state(cls, raw_state, new_version_id):
        '''Convert raw serializable state to metamorphoses-compatible state.

        Args:
            raw_state: state to convert.
            new_version_id: version id of Version to convert raw_state to.

        Returns:
            tuple: State in the format specified by new_version_id.
        '''
        return raw_state

    @classmethod
    def convert(cls, obj, old_version_id, new_version_id):
        '''Convert obj from one version to another

        Args:
            obj: The object to convert.
            old_version_id: The version id of the current Version of obj.
            new_version_id: The version id of the Version to convert obj to.
        '''
        pass

    @classmethod
    def init_factory(cls, version_id=None):
        '''Create an function suitable for use as __init__().

        Args:
            version_id: version id for the Version for which to create the function.

        Returns:
            function: A function suitable for use as __init__().
        '''
        if version_id is None:
            version_id = cls.current_version_id
        version = cls.versions[version_id]
        # Use copies of the default values.
        attrs = [attr for attr in cls.versions[version_id].all_attrs]

        def init(obj, *args, **kwargs):
            if cls.init_bases:
                for base in version.bases:
                    if not base.has_version:
                        base.cls.__init__(obj)

            attrs_dict = dict(copy.deepcopy(attrs))
            # Check for invalid len(args).
            if len(args) + len(kwargs) > len(attrs):
                # FIXME: show the correct class name.
                raise MetamorphosesException(
                    '%s.__init__() accepts %d args; %s were given.'
                    % (obj.__class__.__name__,
                       len(attrs)+1,
                       len(args)+len(kwargs)+1,))

            # Check for invalid kwargs:
            for arg in kwargs:
                if arg not in attrs_dict:
                    # FIXME: show the correct class name.
                    # Is there a builtin exception for this?
                    raise MetamorphosesException(
                        '%s.__init__() does not accept a "%s" argument.'
                        % (obj.__class__.__name__, arg))
            # Add args.
            for ii in range(len(args)):
                attrs_dict[attrs[ii].name] = args[ii]

            # Add kwargs.
            attrs_dict.update(kwargs)

            # Set attributes.
            for arg, value in attrs_dict.items():
                setattr(obj, arg, value)


        return init


    @classmethod
    def decorate(cls, version_id, decorated_cls):
        '''Add various dunder methods to a class.

        Add __getstate__(), __setstate__(), and optionally __init__(),
        __repr__(), and comparison dunder methods given a Version.

        Args:
            version_id: version id for the Version used to create dunder methods.
            decorated_cls: class to receive the dunder methods.
        '''

        if not '__getstate__' in decorated_cls.__dict__ and cls.make_getstate:
            setattr(decorated_cls, '__getstate__',
                    cls.getstate_factory(version_id))

        if not '__setstate__' in decorated_cls.__dict__ and cls.make_setstate:
            setattr(decorated_cls, '__setstate__',
                    cls.setstate_factory(version_id))

        if cls.make_boost_python_compatible:
            setattr(decorated_cls, '__getstate_manages_dict__', True)

        if not '__init__' in decorated_cls.__dict__ and cls.make_init:
            init = cls.get_init()
            setattr(decorated_cls, '__init__', init)

        if not '__repr__' in decorated_cls.__dict__ and cls.make_repr:
            setattr(decorated_cls, '__repr__',
                    lambda obj: cls.repr(version_id, obj))

        # FIXME: If a suitable set of dunder methods exist that allow
        # total_ordering to work, we should not make these, since they
        # will likely be logically incompatible.

        if not '__lt__' in decorated_cls.__dict__ and cls.make_cmp:
            setattr(decorated_cls, '__lt__', lambda lhs, rhs: cls.lt(lhs, rhs))
        if not '__eq__' in decorated_cls.__dict__ and cls.make_cmp:
            setattr(decorated_cls, '__eq__', lambda lhs, rhs: cls.eq(lhs, rhs))

        # FIXME: Figure out why __ne__ generated by total_ordering fails test.
        # Workaround by creating it here.
        if not '__ne__' in decorated_cls.__dict__ and cls.make_cmp:
            setattr(decorated_cls, '__ne__', lambda lhs, rhs: not cls.eq(lhs, rhs))

        if cls.make_cmp:
            decorated_cls = total_ordering(decorated_cls)

        if cls.make_slots:
            dd = decorated_cls.__dict__.copy()
            dd['__slots__'] = \
                (attr.name for attr in cls.versions[version_id].all_attrs)
            decorated_cls = type(decorated_cls.__name__,
                                 decorated_cls.__bases__,
                                 dd)

        return decorated_cls

    @classmethod
    def get_init(cls, version_id=None):
        ''' Override if you need to use a custom __init__() with cls.factory().
        '''
        return cls.init_factory(version_id)

    @classmethod
    def factory(cls, version_id, decorated_cls, *args, **kwargs):
        ''' Create an instance of a class of given version.

        Args:
            version_id: version id of the Version of the object to be created.
            decorated_cls: class of the object to be created.
            *args: variable length argument list passed to __new__ and __init__.
            **kwargs: keyword argument list passed to __new__ and __init__.
        '''
        obj = decorated_cls.__new__(decorated_cls, *args, **kwargs)
        cls.get_init(version_id)(obj, *args, **kwargs)
        return obj

class Attr(namedtuple('Attr', 'name default_value')):
    '''Attribute of an object.

    Args:
        name (str): The name of the attribute.
        default_value: Default value of the attribute.
    '''

class FutureAttr(namedtuple('FutureAttr', 'name default_value')):
    '''Future Attribute of an object.

    Args:
        name (str): The name of the attribute.
        default_value: Default value of the attribute.
    '''



class Base(object):
    '''Reference to a Version of a parent class.

    Args:
        cls: Metamorphoses subclass that decorates the parent class.
        version_id: version_id of the Version of the parent class.
    '''
    def __init__(self, cls, version_id=None):
        self.cls = cls
        self.__version_id = version_id
        self.__attrs = None
        self.__future_attrs = None

    @property
    def has_version(self):
        return self.__version_id is not None

    @property
    def version_id(self):
        if not self.has_version:
            raise MetamorphosesException('Base(%s) has no version_id.' %
                                         self.cls.__name__)
        return self.__version_id

    @property
    def version(self):
        return self.cls.versions[self.version_id]

    @property
    def attrs(self):
        if self.__attrs is None:
            self.__attrs = copy.copy(self.version.attrs)
            for base in self.version.bases:
                if base.has_version:
                    self.__attrs += base.attrs
        return self.__attrs

    @property
    def future_attrs(self):
        if self.__future_attrs is None:
            self.__future_attrs = copy.copy(self.version.future_attrs)
            for base in self.version.bases:
                self.__future_attrs += base.future_attrs
        return self.__future_attrs


    def clear_cache(self):
        '''Clear cached attribute lists.'''
        self.__attrs = None
        self.__future_attrs = None
        for base in self.version.bases:
            base.clear_cache()


class Version(object):
    '''Declaration of all serialized attributes of instances of a class.

    Args:
        *args: Variable length argument list. An argument may be of type
            Version, Base, Attr, or FutureAttr.  Attributes are added
            to self.attrs in the order they are received.  When a
            Version or Base is added to a Version, all attributes (and
            attributes of Bases) of that Version or Base are added
            immediately.

    Attributes:
        attrs (list): List of Attrs.  Includes Attrs of Bases with versions.
        bases (list): List of Bases.  Includes Bases with versions and without.
        future_attrs (list): List of FutureAttrs.  Includes FutureAttrs
            of Bases with versions.
    '''
    def __init__(self, *args):
        attrs = []
        bases = []
        future_attrs = []

        for arg in args:
            if type(arg) is Attr:
                attrs.append(arg)
            elif type(arg) is Base:
                if arg.has_version:
                    for attr in arg.attrs:
                        attrs.append(attr)
                    for attr in arg.future_attrs:
                        future_attrs.append(attr)
                bases.append(arg)
            elif type(arg) is FutureAttr:
                future_attrs.append(arg)
            elif type(arg) is Version:
                attrs += arg.attrs
                attrs += arg.future_attrs # Promote future_attrs to attrs!
                bases += arg.bases

        seen_attrs = set()
        self.attrs = uniq(attrs, lambda aa: aa.name, seen_attrs)
        self.bases = uniq(bases)
        self.future_attrs = uniq(future_attrs, lambda aa: aa.name, seen_attrs)
        self._future_attr_names = None

        # FIXME: Optionally raise exception if there are duplicate
        # attrs (currently screened by uniq()). Or have list of
        # acceptable overlaps.

    @property
    def all_attrs(self):
        for attr in self.attrs:
            yield attr
        for attr in self.future_attrs:
            yield attr

    def future_attr_names(self):
        if self._future_attr_names is None:
            self._future_attr_names = [attr.name for attr in self.future_attrs]
            self._future_attr_names.sort()

        return self._future_attr_names

class MetamorphosesException(Exception):
    pass


def uniq(seq, key=lambda x:x, seen=None):
    if seen is None:
        seen = set()
    seen_add = seen.add
    return [x for x in seq if not (key(x) in seen or seen_add(key(x)))]
